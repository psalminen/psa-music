---
title: Father of the Bride
date: 2019-07-23T11:51:49-06:00
lastmod: 2019-07-23T11:51:49-06:00
draft: false
cover: https://gitlab.com/psalminen/psa-music/raw/master/static/img/fatherofthebride.png
weight: 1
categories: ["albums"]
tags: ["VampireWeekend", "Indie", "Rock", "IndieRock", "2019"]
description: Vampire Weekend - Father of the Bride
---

# Vampire Weekend
## Father of the Bride
### By: Paul Salminen

![Father of the Bride](/img/fatherofthebride.png)

It's starting to grow on me. At first I thought it was too out there, almost jammy for my tastes.
